<body>
<div class="container">
    <header class="blog-header py-3">
    </header>

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">Teste de Desenvolvimento</h1>
            <p class="lead my-3">Teste PHP/JS/HTML para correções de itens com problemas, existe uma imagem que cont�m
                exatamente como deve ficar. Procure a imagem na pasta /img/layout/layout.jpg. Todos os Links devem estar
                funcionando .</p>
        </div>
    </div>

    <div class="row mb-2">
        <?php
        $stringJson = file_get_contents("json/posts.json");
        $jsonPost = json_decode($stringJson);

        foreach ($jsonPost->posts as $key => $post) {

            if ($key == 0) {

                ?>
                <div class="col-md-6">
                    <div class="card flex-md-row mb-4 box-shadow h-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-danger"><?php echo $post->chapeu; ?></strong>
                            <h3 class="mb-0">
                                <a class="text-dark" href="<?php echo $post->link; ?>"><?php echo $post->titulo; ?></a>
                            </h3>
                            <p class="card-text mb-auto"><?php echo $post->linhafina; ?></p>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($key == 1) {

                ?>
                <div class="col-md-6">
                    <div class="card flex-md-row mb-4 box-shadow h-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-success"><?php echo $post->chapeu; ?></strong>
                            <h3 class="mb-0">
                                <a class="text-dark" href="<?php echo $post->link; ?>"><?php echo $post->titulo; ?></a>
                            </h3>
                            <p class="card-text mb-auto"><?php echo $post->linhafina; ?></p>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>

                <div class="col-md-6">
                    <div class="card flex-md-row mb-4 box-shadow h-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-success"><?php echo $post->chapeu; ?></strong>
                            <h3 class="mb-0">
                                <a class="text-dark" href="<?php echo $post->link; ?>"><?php echo $post->titulo; ?></a>
                            </h3>
                            <p class="card-text mb-auto"><?php echo $post->linhafina; ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
        } ?>

    </div>

</div><!-- /.row -->
<?php
include 'footer/index.php';
?>

</main><!-- /.container -->
</body>